<?php
include('functions.php');

$id = $_GET['id'];
if($id) {
  $user = getUser($id);
  if($user) {
    $deleted = deleteUsers($id);
    if($deleted) {
      header('Location: /auth/?status=success');
    } else {
      header('Location: /auth/?status=error');
    }
  } else {
    header('Location: /auth/?status=error');
  }
} else {
  header('Location: /auth/index.php');
}
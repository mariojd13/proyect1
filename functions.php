<?php

/**
 *  Gets a new mysql connection
 */
function getConnection() {
  $connection = new mysqli('localhost', 'root', 'root', 'mytree');
  if ($connection->connect_errno) {
    printf("Connect failed: %s\n", $connection->connect_error);
    die;
  }
  return $connection;
}

/**
 * Inserts a new student to the database
 *
 * @student An associative array with the student information
 */
function saveStudent($student) {
  $conn = getConnection();
  $sql = "INSERT INTO students( `full_name`, `email`, `document`)
          VALUES ('{$student['full_name']}', '{$student['email']}', '')";
  $conn->query($sql);

  if ($conn->connect_errno) {
    $conn->close();
    return false;
  }
  $conn->close();
  return true;
}


/**
 * Get all students from the database
 *
 */
function getUsers(){
  $conn = getConnection();
  $sql = "SELECT * FROM users";
  $result = $conn->query($sql);

  if ($conn->connect_errno) {
    $conn->close();
    return [];
  }
  $conn->close();
  return $result;
}

function getUser($id){
  $conn = getConnection();
  $sql = "SELECT * FROM users WHERE id = $id";
  $result = $conn->query($sql);

  if ($conn->connect_errno) {
    $conn->close();
    return [];
  }
  $conn->close();
  return $result;
}

/**
 * Get all Trees from the database
 *
 */
function getTrees(){
  $conn = getConnection();
  $sql = "SELECT * FROM trees";
  $result = $conn->query($sql);

  if ($conn->connect_errno) {
    $conn->close();
    return [];
  }
  $conn->close();
  return $result;
}

function getTree($id){
  $conn = getConnection();
  $sql = "SELECT * FROM trees WHERE id = $id";
  $result = $conn->query($sql);

  if ($conn->connect_errno) {
    $conn->close();
    return [];
  }
  $conn->close();
  return $result->fetch_array();
}



function getCountry(){
  $conn = getConnection();
  $sql = "SELECT * FROM paises";  
  $result = $conn->query($sql);

  if ($conn->connect_errno) {
    $conn->close();
    return [];
  }
  $conn->close();
  return $result;
}

/**
 * Get one specific student from the database
 *
 * @id Id of the student
 */
function authenticate($email, $password){
  $conn = getConnection();
  $sql = "SELECT * FROM users WHERE `email` = '$email' AND `password` = '$password'";
  $result = $conn->query($sql);

  if ($conn->connect_errno) {
    $conn->close();
    return false;
  }
  $conn->close();
  return $result->fetch_array();
}

/**
 * Deletes an tree from the database
 */
function deleteTree($id){
  $conn = getConnection();
  $sql = "DELETE FROM trees WHERE id = $id";
  $result = $conn->query($sql);

  if ($conn->connect_errno) {
    $conn->close();
    return false;
  }
  $conn->close();
  return true;
}

function saveUser($user){
  $conn = getConnection();
  $sql = "INSERT INTO users( `full_name`, `phone`, `email`, `password`, `country`)
          VALUES ('{$user['full_name']}', '{$user['phone']}', '{$user['email']}','{$user['password']}','{$user['country']}')";
  $conn->query($sql);

  if ($conn->connect_errno) {
    $conn->close();
    return false;
  }
  $conn->close();
  return true;
  
  echo $sql;

  
}

function updateTrees($tree) {
  $conn = getConnection();
  $sql = "UPDATE trees set `id` = '{$tree['id']}' , `name` = '{$tree['name']}',
    `profilePic` = '{$tree['profilePic']}' , `length` = '{$tree['length']}' WHERE `id` = {$tree['id']}";

  $conn->query($sql);

  if ($conn->connect_errno) {
    $conn->close();
    return false;
  }
  $conn->close();
  return true;
}

function uploadPicture($inputName){
  $fileObject = $_FILES[$inputName];

  $target_dir = "images/";
  $target_file = $target_dir . basename($fileObject["name"]);
  $uploadOk = 0;
  if (move_uploaded_file($fileObject["tmp_name"], $target_file)) {
    return $target_file;
  } else {
    return false;
  }
}

function deleteUsers($id){
  $conn = getConnection();
  $sql = "DELETE FROM users WHERE id = $id";
  $result = $conn->query($sql);

  if ($conn->connect_errno) {
    $conn->close();
    return false;
  }
  $conn->close();
  return true;
}

function getTreesCR(){
  $conn = getConnection();
  $sql = "SELECT * FROM treesCR";
  $result = $conn->query($sql);

  if ($conn->connect_errno) {
    $conn->close();
    return [];
  }
  $conn->close();
  return $result;
}

function getTreeCR($id){
  $conn = getConnection();
  $sql = "SELECT * FROM treesCR WHERE id = $id";
  $result = $conn->query($sql);

  if ($conn->connect_errno) {
    $conn->close();
    return [];
  }
  $conn->close();
  return $result->fetch_array();
}

function saveTree($user) {
    $conn = getConnection();
    $sql = "INSERT INTO TREES (`name`, `owner`, `id_owner`, `length`)
            VALUES ('{$tree['name']}', '".$_SESSION['$owner']."', '', '')";
    $conn->query($sql);

if ($conn->connect_errno) {
  $conn->close();
  return false;
}
$conn->close();
return true;
}


<?php
  include('functions.php');
  include('navbar.php');
  
  //include();
  session_start();
  
  //$email = $_SESSION['email'];
  //if (!$email) {
  //  header('Location: /auth/index.php');
  //}
$user=$_SESSION['user'];
if(!$user){
	header('Location: /auth/index.php');
}

  ?>
<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<html>
<head>
	<title>My Tree</title>
   <!--Made with love by Mutiullah Samim -->
   
	<!--Bootsrap 4 CDN-->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    
    <!--Fontawesome CDN-->
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

	<!--Custom styles-->
	<link rel="stylesheet" type="text/css" href="style.css">
</head>


  <h1> Bienvenido <?php echo $user['full_name'] ?> </h1>
  <a href="/auth/logout.php">Logout</a>

  <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" />
<meta charset="utf-8">

  <body>
    <div class="container sinpadding">

      <div class="row">
        <div class="col-md-6">
          <div class="card">
            <div class="texto 1">
              <h1>Friends</h1>
              <h4>These are the registered friends</h4>
              <table class="table table-light">
              <tbody>
                <tr>
                  <td>Id</td>
                  <td>Full Name</td>
                  <td>Country</td>
                  <td>Actions</td>
                </tr>
                <?php
                  $users = getUsers();
                  $usersHtml = "";
                  foreach ($users as $user) {
                    $usersHtml .= "<tr id='tree_{$user['id']}'><td>{$user['id']}</td><td>{$user['full_name']}</td><td>{$user['country']}</td></td></tr><td><a href='edit.php?id={$user['id']}'>Edit</a> | 
                    <a href='deleteUsers.php?id={$user['id']}'>Delete</a></td></tr>";
                  }
                  echo $usersHtml;
                ?>
              </tbody>
            </table>
            </div>
          </div>
        </div>
        <div class="col-md-6">
          <div class="card">
            <div id="texto">
              <h1>Trees</h1>
              <h4>These are the registered trees</h4>
              <table class="table table-light">
              <tbody>
                <tr>
                  <td>Name</td>
                  <td>Owner</td>
                  <td>Length</td>
                  <td>Actions</td>
                </tr>
                <?php
                  $trees = getTrees();
                  $treesHtml = "";
                  foreach ($trees as $tree) {
                    $treesHtml .= "<tr id='tree_{$tree['id']}'><td>{$tree['name']}</td><td>{$tree['owner']}</td><td>{$tree['length']}</td></td><td><a href='edit.php?id={$tree['id']}'>Edit</a> | 
                    <a href='deleteTree.php?id={$tree['id']}' class='btn btn-primary'>Delete</a></td></tr>";
                  }
                  echo $treesHtml;
                ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </body>
</html>
<?php
  include('functions.php');

  if($_REQUEST['id']) {
    $tree = getTree($_REQUEST['id']);
  }

  // if editing
  if($_POST){
    if ($filename = uploadPicture('picture')){
      
        //now that we upload we can save the tree
        $tree['profilePic'] = $filename;
        $tree['name'] = $_POST['name'];
        $tree['length'] = $_POST['length'];
        $tree['id'] = $_POST['id'];
        updateTrees($tree);
      
    } else {
      echo "There was an error saving the picture";
    }
  }


  // else {
  //   // header('Location: /workshop03/?status=error');
  // }
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">

  <!-- Latest compiled and minified CSS -->
  <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">

  <title>My Tree</title>
</head>
<body>
<div class="container">
    <div class="msg">
    </div>
    <h1>Edit Conditions of Friend</h1>
    <form method="POST" class="form-inline" role="form" enctype="multipart/form-data">
      <input type="hidden" name="id" value="<?php echo $tree['id']?>">
      <div class="form-group">
        <label class="sr-only" for="">Name</label>
        <input type="text" class="form-control" id="" name="name" placeholder="Name" value="<?php echo $tree['name'] ?>">
      </div>
      <div class="form-group">
        <label class="sr-only" for="">Owner</label>
        <input type="text" class="form-control" id="" name="owner" placeholder="Owner" value="<?php echo $tree['owner'] ?>">
      </div>
      <div class="form-group">
        <label class="sr-only" for="">Length</label>
        <input type="text" class="form-control" id="" name="length" placeholder="Length" value="<?php echo $tree['length'] ?>">
      </div>

      <input type="file" name="picture" id="picture">
      <img src="<?php echo $tree['profilePic']?>"></img>
      <button type="submit" class="btn btn-primary">Save</button>
      <a href='index.php' class="btn btn-primary"> Home</a>
    </form>
</div>

</body>
</html>


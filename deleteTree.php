<?php
include('functions.php');

$id = $_GET['id'];
if($id) {
  $tree = getTrees($id);
  if($tree) {
    $deleted = deleteTree($id);
    if($deleted) {
      header('Location: /auth/?status=success');
    } else {
      header('Location: /auth/?status=error');
    }
  } else {
    header('Location: /auth/?status=error');
  }
} else {
  header('Location: /auth/index.php');
}
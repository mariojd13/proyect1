<?php
  include('functions.php');

  if(!empty($_REQUEST['id'])) {
    $saved = saveTree($_REQUEST);

    if($saved) {
      header('Location: /auth/buyPage.php/?status=success');
    } else {
      header('Location: /auth/buyPage.php/?status=error');
    }
  } else {
    header('Location: /auth/buyPage.php/?status=error');
  }

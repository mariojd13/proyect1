<?php
include('navbar.php');
include('functions.php');
  
  //include();
  session_start();
  
  //$email = $_SESSION['email'];
  //if (!$email) {
  //  header('Location: /auth/index.php');
  //}
$user=$_SESSION['user'];
if(!$user){
	header('Location: /auth/index.php');
}

?>
<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="jquery-3.1.1.min.js"></script>
<script src="bootstrap/js/bootstrap.js"></script>
<script src="assets/js/jquery-1.11.0.min.js"></script>
<script src="js/bootstrap.js"></script>
<link rel="stylesheet" href="css/bootstrap.min.css">
<script>
$(document).ready(function(){
    $(.mimodalejemplo).on('click'.function()
});
</script>

<!------ Include the above in your HEAD tag ---------->

<!DOCTYPE html>
<html>
<head>
	<title>My Tree</title>
   <!--Made with love by Mutiullah Samim -->
   
	<!--Bootsrap 4 CDN-->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    
    <!--Fontawesome CDN-->
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

	<!--Custom styles-->
	<link rel="stylesheet" type="text/css" href="style.css">
    <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css">
</head>

<h1> Bienvenido <?php echo $user['full_name'] ?> </h1>
  <a href="/auth/logout.php">Logout</a>

<body>
<div id="container"> 


<h1>Product List</h1> 
<form action="/auth/insertTree.php" method="POST" class="form-inline" role="form">
    <div class="col-md-8">
            <div id="texto">
              <h1>Trees</h1>
              <h4>Please select one or several trees to your list.</h4>
              <table class="table table-dark">
              <tbody>
                <tr>
                  <td>Species</td>
                  <td>Name</td>
                  <td>Commentary</td>
                  <td>Price</td>
                  <td>Actions</td>
                </tr>
                <?php
                $treesCR = getTreesCR();
                  $treesHtml = "";
                  foreach ($treesCR as $treeCR) {
                    $treesHtml .= "<tr id='tree_{$treeCR['id']}'><td>{$treeCR['species']}</td><td>{$treeCR['name']}</td><td>{$treeCR['commentary']}</td><td>{$treeCR['price']}</td></td>
                    <td><a href='/auth/insertTree.php?id={$treeCR['id']}'>Add</a> | <a href='#'>View</a></td></tr>";
                    //<a href='details.php?id={$treeCR['id']}'>View</a></td></tr>"
                  }
                  echo $treesHtml;
                ?>
              </tbody>
            </table>
  </form>
            <div class="col-md-8">
              <div id="texto">
              <h1>Your trees</h1>
              <h4>These are the trees registered in his name</h4>
              <table class="table table-light">
              <tbody>
                <tr>
                  <td>Name of tree</td>
                  <td>Owner</td>
                </tr>
                <?php
                  $trees = getTrees();
                  $treesHtml = "";
                  foreach ($trees as $tree) {
                    $treesHtml .= "<tr id='tree_{$tree['id']}'><td>{$tree['name']}</td><td>{$tree['owner']}</td></td></tr>";
                  }
                  echo $treesHtml;
                ?>
              </tbody>
            </table>
          </div>
          </div>
        </div>
        
</div>

</body>
</html>
